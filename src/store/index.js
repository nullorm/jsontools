import Vue from 'vue'
import Vuex from 'vuex'

import * as dashboard from './modules/dashboard'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    dashboard
  },
  strict: true
})
