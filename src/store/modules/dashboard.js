import * as types from '../mutation-types'
import { sortObject } from '@/utils'

export const state = {
    stepStack: [],
    stepIndex: -1
}
export const getters = {
    currentStep: state => state.stepStack[state.stepIndex] || {},
    stepIndex: state => state.stepIndex,
    steps: state => state.stepStack
}
export const actions = {}

export const mutations = {
    [types.addJSON](state, { json }) {
        const time = Date.now().toString()
        const mutation = types.addJSON
        const title = state.stepIndex > 0 ? 'edited json' : 'added json'
        const nextStep = { json, mutation, time, title }
        try {
            nextStep.data = JSON.parse(json)
            nextStep.valid = true
        } catch (_) {
            nextStep.error = 'Unable to parse json'
            nextStep.invalid = true
        }
        const currentStep = state.stepStack[state.stepIndex]
        const currentMutation = currentStep && currentStep.mutation
        if (mutation != currentMutation) {
            state.stepIndex += 1
        } else if (currentStep) {
            state.stepStack.pop()
        }
        state.stepStack.splice(state.stepIndex, state.stepStack.length - state.stepIndex)
        state.stepStack[state.stepIndex] = nextStep
    },
    [types.beautifyJSON](state) {
        const currentStep = state.stepStack[state.stepIndex]
        const mutation = types.beautifyJSON
        if (mutation != currentStep.mutation) {
            const time = Date.now().toString()
            const json = JSON.stringify(currentStep.data, null, 4)
            const title = 'beautified json'
            const nextStep = { data: currentStep.data, json, mutation, time, title, valid: true }
            state.stepIndex += 1
            state.stepStack.splice(state.stepIndex, 0, nextStep)
        }
    },
    [types.compressJSON](state) {
        const currentStep = state.stepStack[state.stepIndex]
        const mutation = types.compressJSON
        if (mutation != currentStep.mutation) {
            const time = Date.now().toString()
            const json = JSON.stringify(currentStep.data)
            const title = 'compressed json'
            const nextStep = { data: currentStep.data, json, mutation, time, title, valid: true }
            state.stepIndex += 1
            state.stepStack.splice(state.stepIndex, 0, nextStep)
        }
    },
    [types.gotoStep](state, { index }) {
        state.stepIndex = index
    },
    [types.sortJSON](state) {
        const currentStep = state.stepStack[state.stepIndex]
        const mutation = types.sortJSON
        if (mutation != currentStep.mutation) {
            const time = Date.now().toString()
            const data = sortObject(currentStep.data)
            const json = JSON.stringify(data)
            const title = 'sorted json by keys'
            const nextStep = { data, json, mutation, time, title, valid: true }
            state.stepIndex += 1
            state.stepStack.splice(state.stepIndex, state.stepStack.length - state.stepIndex, nextStep)
        }
    }
}
