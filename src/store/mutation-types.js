export const addJSON = 'addJSON'
export const beautifyJSON = 'beautifyJSON'
export const compressJSON = 'compressJSON'
export const gotoStep = 'gotoStep'
export const sortJSON = 'sortJSON'
