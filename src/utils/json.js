export function sortObject(data) {
    if (data.constructor === Object) {
        return Object.keys(data)
            .sort()
            .reduce((acc, k) => ({ ...acc, [k]: sortObject(data[k]) }), {})
    } else if (data instanceof Array) {
        return data.map(sortObject)
    }
    return data
}
